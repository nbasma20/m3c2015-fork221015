!subroutine for computing mean of an array, f
!f should be double precision with size, N
!The mean is returned via the variable, meanf.
subroutine fmean(N,f,meanf)
    implicit none
    integer, intent(in) :: N
    real(kind=8), dimension(N), intent(in) :: f
    real(kind=8), intent(out) :: meanf

        meanf = sum(f)/dble(N)

end subroutine fmean
	
