!Test code for statsmod
!To compile: gfortran -fopenmp -o lab7 statsmod2.f90 lab7.f90
!To run: ./test
program test
    use stats
    use omp_lib
    implicit none
    integer :: i1,j1,M,N,numThreads
    integer(kind=8) :: s1,s2,rate
    real(kind=8) :: meanf,varf,Amean_tot,t1,t2
    real(kind=8), allocatable, dimension(:) :: f,Amean,Avar
    real(kind=8), allocatable, dimension(:,:) :: A1,A2


    !read in problem parameters
    open(unit=10,file='data.in')
        read(10,*) M
        read(10,*) N
!$      read(10,*) numThreads
    close(10)


    !initialize variables
    Nstats = N
    allocate(A1(M,N),A2(M,N),f(N),Amean(M),Avar(M))
    Amean_tot=0.d0

    !Generate random matrices
    call random_number(A1)
    call random_number(A2)

    !Compute means and variances of A1
    call system_clock(s1)
    do i1=1,M
        f = A1(i1,:)
        Amean(i1) = compute_mean(f)
        call compute_var(f,Amean(i1),Avar(i1))
        Amean_tot = Amean_tot + Amean(i1)
    end do
    Amean_tot = Amean_tot/dble(M)
    call system_clock(s2,rate)

    !report results
    print *, 'wall time:',float(s2-s1)/float(rate)
    print *, 'Amean_tot=',Amean_tot
    print *, 'Test:',Amean_tot - sum(sum(A1,1))/dble(M*N)

end program test
